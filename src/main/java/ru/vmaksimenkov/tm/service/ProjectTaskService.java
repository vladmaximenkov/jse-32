package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
    }

    @Override
    public void clearTasks(@NotNull final String userId) {
        taskRepository.removeAllBinded(userId);
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!taskRepository.existsByProjectId(userId, projectId)) return null;
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
        @Nullable final String projectId = projectRepository.getIdByIndex(userId, projectIndex);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByIndex(userId, projectIndex);
    }

    @Override
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        @Nullable final String projectId = projectRepository.getIdByName(userId, projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByName(userId, projectName);
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.unbindTaskFromProject(userId, taskId);
    }

}
