package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final E e) {
        if (e == null) throw new ProjectNotFoundException();
        repository.add(e);
    }

    @Override
    public void addAll(@Nullable List<E> list) {
        repository.addAll(list);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public void remove(@Nullable final E e) {
        if (e == null) throw new ProjectNotFoundException();
        repository.remove(e);
    }

    @Override
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        repository.removeById(userId, id);
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public int size(@NotNull final String userId) {
        if (repository.size() == 0) return 0;
        return repository.size(userId);
    }

}
