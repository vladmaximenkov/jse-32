package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
