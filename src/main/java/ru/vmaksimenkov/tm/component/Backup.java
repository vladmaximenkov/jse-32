package ru.vmaksimenkov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.command.data.BackupLoadCommand;
import ru.vmaksimenkov.tm.command.data.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private final int interval;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getBackupInterval();
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    public void run() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, interval, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}
