package ru.vmaksimenkov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.service.ServiceLocator;
import ru.vmaksimenkov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected ServiceLocator serviceLocator;

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @NotNull
    public abstract String commandName();

    @Nullable
    public Role[] commandRoles() {
        return null;
    }

    public abstract void execute();

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}