package ru.vmaksimenkov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.Domain;

import java.io.FileOutputStream;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Save data to YAML file";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-yaml-save";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
